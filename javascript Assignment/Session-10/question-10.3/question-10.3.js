function charFreq(arr)
{
var array_lengths={};
for(var i=0;i<arr.length;i++){
    var value=arr[i];
    if(value in array_lengths){
        array_lengths[value]++;
    }

    else{
        array_lengths[value]=1;
    }
}

return array_lengths;

}

var arr = prompt("Enter your numbers").split(",")
var counter=charFreq(arr);
console.log(counter);